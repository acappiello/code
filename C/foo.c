#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <assert.h>
#include <time.h>

int main() {
  int sum = 0;
  int trials = 10000000;
  srand(time(NULL));

  for (int i = 0; i < trials; i++) {
    int x1 = rand() % 2;
    int x2 = rand() % 2;
    int x3 = rand() % 2;
    int x4 = rand() % 2;
    int x5 = rand() % 2;
    
    int res = (x1 || x3) + (x2 || !x4) + (!x2 || !x5) + (x3 || x4) +
      (!x1 || x4);

    sum += res;
  }

  printf("--%f\n", (float)sum/(float)trials);
  
  return 0;
}
  
