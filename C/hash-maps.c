/**
 ******************************************************************************
 *                    15-122 Principles of Imperative Computation, Fall 2011
 ******************************************************************************
 *     Dynamic hash _map_ with separate chaining:
                                      array[h(key)] = value
 *     Default value of the load factor is 2.
 *     The table size is doubled up if LF>=2;
 *     The table is never resized on deletion.
 *
 * Victor Adamchik
 ******************************************************************************/
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "contracts.h"
#include "hash-maps.h"

struct list_node
{
	ktype key;
	vtype value;
	list next;
};

struct hmap_record
{
	int size;    // the number of inserted items, i coul dbe bigger than length
	int length;  // the table length
	list* table;
	int (*hash_function)(ktype k);
	bool (*key_equal)(ktype k1, ktype k2);
	void (*key_free)(ktype k);
	void (*value_free)(vtype v);
};


/*******************************
                 HASH MAP:      table[h(key)] = value
 *******************************/

// Creates a new node and appends it to tail
list list_new(ktype key, vtype value, list tail)
{
	list lnew = malloc(sizeof(struct list_node));
	lnew->key = key;
	lnew->value = value;
	lnew->next = tail;
	return lnew;
}

// Returns a hash code for the key modulo a table length
int hash(ktype key, hmap H)
{
	int h = H->hash_function(key);
	h = h % H->length;           /* reduce to range */
	if (h < 0) h += H->length;  /* make positive, if necessary */
	//@ENSURES 0 <= h && h < H->length;
	return h;
}

//hashmap invariants
bool is_hmap(hmap H)
{
	if (H == NULL) return false;
	if (H->length == 0) return false;

	for (int k = 0; k < H->length; k++)
    //@loop_invariant 0 <= k && k <= H->length;
    {
		list L = H->table[k];
		while (L != NULL)
		{
			if (hash(L->key, H) != k) return false;
			L = L->next;
		}
	}

	return true;
}

// Creates an empty hashmap
hmap hmap_new(size_t init_length,
		int (*hash_function)(ktype k),
		bool (*key_equal)(ktype k1, ktype k2),
		void (*key_free)(ktype k),
		void (*value_free)(vtype v))
{
	hmap H = malloc(sizeof(struct hmap_record));
	H->table = calloc(init_length, sizeof(struct list_node));
	H->size = 0;
	H->length = init_length;
	H->hash_function = hash_function;
	H->key_equal = key_equal;
	H->key_free = key_free;
	H->value_free = value_free;
	//@ENSURES is_hmap(H);
	return H;
}


// Returns the current table size
int hmap_size(hmap H)
{
	//@REQUIRES is_hmap(H);
	return H->size;
}

// Returns true if the key is in the map
bool hmap_contains_key(ktype key, hmap H)
{
	//@REQUIRES is_hmap(H);
	int h = hash(key, H);
	list L = H->table[h];
	while (L != NULL)
	//@loop_invariant is_chain(L, h, H->length);
	{
		if (H->key_equal(L->key, key)) return true;
		L = L -> next;
	}
	return false;
}

// Removes the specified key from this map
// Returns the value, or NULL otherwise
vtype hmap_remove(ktype key, hmap H)
{
	//@REQUIRES is_hmap(H);
	int h = hash(key, H);
	list cur = H->table[h];
	list prev = NULL;
	while (cur != NULL && !H->key_equal(cur->key, key))
	{
		prev = cur;
		cur = cur -> next;
	}
	//delete cur node
	if(cur == NULL) return NULL; //nothing to delete

	H->size--;
	if(prev == NULL)
		H->table[h] = cur->next;
	else
		prev->next = cur->next;

	vtype tmp = cur->value;

	//destroy that node
	if(H->key_free != NULL && cur->key != NULL) H->key_free(cur->key);
	free(cur);

	return tmp;
}


// Returns the load factor
float load_factor(hmap H)
{
	return H->size/((float)H->length);
}

void hmap_free_table(hmap H)
{
	for(int k = 0; k < H->length; k++)
	{
		list L = H->table[k];

		while(L != NULL)
		{
			list tmp = L->next;
			free(L);
			L = tmp;
		}
	}
	free(H->table);
}


void hmap_free(hmap H)
{
	for(int k = 0; k < H->length; k++)
	{
		list L = H->table[k];
		while(L != NULL)
		{
			list tmp = L->next;
			if (H->key_free != NULL && L->key != NULL) H->key_free(L->key);
			if (H->value_free != NULL && L->value != NULL) H->value_free(L->value);
			free(L);
			L = tmp;
		}
	}
	free(H->table);
	free(H);
}
void hmap_resize(hmap H, int new_length)
{
	//@REQUIRES is_hmap(H);
	//@REQUIRES H->length < new_length;
	hmap newmap = hmap_new(new_length, H->hash_function, H->key_equal, H->key_free, H->value_free);

	//rehashing to a new table
	for(int k = 0; k < H->length; k++)
	{
		list L = H->table[k];
		while(L != NULL)
		{
			hmap_insert(L->key, L->value, newmap);
			L = L->next;
		}
	}

	hmap_free_table(H); //delete an old table

	H->table = newmap->table;
	H->length = new_length;
	H->size = newmap->size;
	free(newmap);
	//@ENSURES load_factor(H) <= 2;
	//@ENSURES is_hmap(H);
}

// Inserts the key into the table
// If this map already contains the key, returns false, otherwise - true.
bool hmap_insert(ktype key, vtype value, hmap H)
{
	//@REQUIRES is_hmap(H);
	if(2 <= load_factor(H) ) hmap_resize(H, 2*H->length);
	int h = hash(key, H);
	list L = H->table[h];

	while (L != NULL)
	{
		if (H->key_equal(L -> key, key))
		{
// who is responsible for freeing value???			H->value_free(L->value);
			L->value = value;     /* modify in place if key is already there */
			return false;
		}
		L = L -> next;
	}
	/* key is not in the hash table */
	/* insert it into the chain at A[h] */
	H->size++;
	H->table[h] = list_new(key, value, H->table[h]);

	//@ENSURES load_factor(H) < 2;
	//@ENSURES is_hmap(H);
	return true;
}

// Returns the value, O(1) complexity
vtype hmap_get(ktype key, hmap H)
{
	//@REQUIRES is_hmap(H);
	int h = hash(key, H);
	list L = H->table[h];
	while (L != NULL && !H->key_equal(L -> key, key)) { L = L -> next;}

	if (L == NULL)
		return NULL;
	else
		return L->value;
}
