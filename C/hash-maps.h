/**
 ******************************************************************************
 *                    HOMEWORK  15-122
 ******************************************************************************
 *   	A hash map interface
 *
 *Victor Adamchik
 ******************************************************************************/
#ifndef _HASHTABLE_H_
#define _HASHTABLE_H_

typedef void* ktype;  //this is a key type
typedef void* vtype;  //this is a value type

typedef struct hmap_record* hmap;
typedef struct list_node* list;


bool is_hmap(hmap H);
bool hmap_contains_key(ktype key, hmap H);       /* O(1) avg. */
bool hmap_insert(ktype key, vtype value, hmap H);     /* O(1) avg. */
vtype hmap_remove(ktype key, hmap H);      /* O(1) avg. */
vtype hmap_get(ktype key, hmap H);      /* O(1) avg. */
int hmap_size(hmap H);
void print_hmap(hmap H);

hmap hmap_new(size_t init_size,
			int (*hash_function)(ktype k),
			bool (*key_equal)(ktype k1, ktype k2),
			void (*key_free)(ktype k),
			void (*value_free)(vtype v)
);

void hmap_free(hmap H);


#endif