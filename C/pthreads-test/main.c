#include <stdio.h>
#include <pthread.h>

void *run(void *in) {
  printf("Hello World!\n");
  return NULL;
}

int main (int argc, char **argv) {
  pthread_t tid;
    
  pthread_create(&tid, NULL, &run, NULL);
  pthread_join(tid, NULL);
    
  return 0;
}
