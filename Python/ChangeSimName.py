"""
Sims 4 Change Name Console Command
by Scumbumbo @ ModTheSims
"""
import sims4.commands
import sims.sim_info
import services

def csn_show_usage(output):
    output("usage: rename OldFirst OldLast NewFirst NewLast")
    output("    or rename NewFirst NewLast")
    
@sims4.commands.Command('rename', command_type=sims4.commands.CommandType.Live)
def change_sim_name(fname1=None, lname1=None, fname2=None, lname2=None, _connection=None):
    output = sims4.commands.CheatOutput(_connection)

    if fname1 is None or lname1 is None:
        csn_show_usage(output)
        return False
    else:
        if fname2 is None and lname2 is None:
            new_fname = fname1.capitalize()
            new_lname = lname1.capitalize()
            new_info = services.sim_info_manager().get_sim_info_by_name(new_fname, new_lname)
            tgt_client = services.client_manager().get(_connection)
            if tgt_client is not None:
                old_info = tgt_client.active_sim.sim_info
                old_fname = old_info.first_name
                old_lname = old_info.last_name
            else:
                output("Error: no sim targeted")
                csn_show_usage()
                return False
        elif fname2 is None or lname2 is None:
            csn_show_usage(output)
            return False
        else:
            old_fname = fname1
            old_lname = lname1
            old_info = services.sim_info_manager().get_sim_info_by_name(old_fname, old_lname)
            if old_info is None:
                output("Sim {} {} not found".format(old_fname, old_lname))
                return False
            new_fname = fname2.capitalize()
            new_lname = lname2.capitalize()
            new_info = services.sim_info_manager().get_sim_info_by_name(new_fname, new_lname)

    if new_info is not None:
        output("Error: There is already a sim named {} {}!".format(new_fname, new_lname))
        return False
        
    output("Changing {} {} to {} {}".format(old_fname, old_lname, new_fname, new_lname))
    old_info.first_name = new_fname
    old_info.last_name = new_lname

    old_info.save_sim
