def binsearch(A, x):
    lower = 0
    upper = len(A)

    while lower < upper:
        mid = lower + ((upper - lower) / 2)
        if A[mid] == x:
            return mid
        elif x < A[mid]:
            upper = mid
        else:
            lower = mid + 1
    return None
