# Mistake: not handling None...everywhere.
class BST:
    class TreeNode:
        def __init__(self, k, v):
            self.key = k
            self.value = v
            self.left = None
            self.right = None

        def find(self, k):
            if self.key == k:
                return self
            elif k < self.key and self.left == None:
                return self
            elif k < self.key:
                return self.left.find(k)
            elif k > self.key and self.right == None:
                return self
            else:
                return self.right.find(k)

        def join(self, right):
            curr = self
            while curr.right != None:
                curr = curr.right
            curr.right = right

        def delete(self, k):
            if self.key == k:
                if self.left == None:
                    return self.right
                else:
                    return self.left.join(self.right)
            elif k < self.key and self.left == None:
                return self
            elif k < self.key:
                self.left = self.left.delete(k)
                return self
            elif k > self.key and self.right == None:
                return self
            else:
                self.right = self.right.delete(k)
                return self

        def insert(self, k, v):
            node = self.find(k)
            if node.key == k:
                node.value = v
            elif node.left == None and node.key > k:
                node.left = BST.TreeNode(k, v)
            else:
                node.right = BST.TreeNode(k, v)

        def toString(self):
            if self == None:
                return "nil"
            left = "nil" if self.left == None else self.left.toString()
            right = "nil" if self.right == None else self.right.toString()
            return "Node(" + str(self.key) + ", " + str(self.value) + ", " + \
                left + ", " + right + ")"

        def traverse(self, f):
            if self.left != None:
                self.left.traverse(f)
            f(self.key, self.value)
            if self.right != None:
                self.right.traverse(f)

    def __init__(self):
        self.root = None

    def find(self, k):
        if self.root == None:
            return None

        return self.root.find(k)

    def find_elem(self, k):
        node = self.find(k)
        if node == None:
            return None
        return node.value

    def toString(self):
        if self.root == None:
            return "BST(nil)"
        return "BST(" + self.root.toString() + ")"

    def delete(self, k):
        if self.root == None:
            return False

        # Mistake: forgot to set self.root = ... misses root deleted.
        self.root = self.root.delete(k)
        return True

    def insert(self, k, v):
        if self.root == None:
            self.root = BST.TreeNode(k, v)
        else:
            self.root.insert(k, v)

    def traverse(self, f):
        if self.root == None:
            return
        else:
            self.root.traverse(f)

def printer(k, v):
    print k, v

#def main():
t = BST()
t.root = BST.TreeNode(4, 5)
t.root.left = BST.TreeNode(1, 2)
t.root.right = BST.TreeNode(5, 6)
t.root.left.right = BST.TreeNode(2, 2)
print t.toString()

t.insert(10, 11)
print t.toString()

t.delete(1)
print t.toString()

s = BST()
for i in xrange(10):
    s.insert(i, i)
for i in xrange(0, 10, 2):
    s.delete(i)
print s.toString()
s.traverse(printer)

#if __name__ == "__main__":
#    main()
