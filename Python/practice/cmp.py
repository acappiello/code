class Test:
    def __init__(self, x):
        self.x = x

    def __eq__(self, other):
        print "eq:", self.x, other.x
        return self.x == other.x

    def __lt__(self, other):
        print "lt:", self.x, other.x
        return self.x < other.x


class Test2:
    def __init__(self, x):
        self.x = x

    def __eq__(self, other):
        return self.x == other.x
