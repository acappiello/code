class List:
    class Node:
        def __init__(self, data):
            self.data = data
            self.next = None

    def __init__(self):
        self.front = List.Node(None)
        self.back = self.front

    def push(self, elem):
        new = List.Node(elem)
        new.next = self.front
        self.front = new

    def pop(self):
        if self.front == self.back:
            return None
        elem = self.front.data
        self.front = self.front.next
        return elem

    # Mistake: put elem into new.data, instead of back.data.
    def enqueue(self, elem):
        new = List.Node(None)
        self.back.data = elem
        self.back.next = new
        self.back = new

    def dequeue(self):
        return self.pop()

    def toString(self):
        res = ""
        point = self.front
        while point.next != None:
            res += str(point.data) + " -> "
            point = point.next
        res += "X"
        return res

def main():
    l = List()
    for i in xrange(10):
        l.push(i)
    print l.toString()
    for i in xrange(10):
        l.enqueue(100 + i)
    print l.toString()
    for i in xrange(5):
        print l.pop()
    print l.toString()

if __name__ == "__main__":
    main()
