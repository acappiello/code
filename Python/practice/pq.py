class PQ:
    def __init__(self):
        self.elems = [0]

    def peek(self):
        if len(self.elems) < 2:
            return None
        return self.elems[1]

    def insert(self, elem):
        self.elems.append(elem)
        i = len(self.elems)-1
        while i > 0 and self.elems[i] < self.elems[i/2]:
            tmp = self.elems[i]
            self.elems[i] = self.elems[i/2]
            self.elems[i/2] = tmp
            i /= 2

    def delmin(self):
        # Mistake: initially forgot this edge case.
        if len(self.elems) < 2:
            return None

        elem = self.elems[1]
        self.elems[1] = self.elems[-1]
        self.elems.pop()
        i = 1
        while (2*i < len(self.elems) and self.elems[i] > self.elems[2*i]) or \
              (2*i+1 < len(self.elems) and self.elems[i] > self.elems[2*i+1]):
            if len(self.elems) == 2*i+1 or self.elems[2*i] < self.elems[2*i+1]:
                j = 2*i
            else:
                j = 2*i+1
            tmp = self.elems[i]
            self.elems[i] = self.elems[j]
            self.elems[j] = tmp
            # Mistake: forgot the following line.
            i = j
        return elem

    def toString(self):
        res = ""
        i = 1
        # Mistake: had following line inside outer loop.
        j = 1
        while i < len(self.elems):
            k = 0
            while i < len(self.elems) and k < j:
                res += str(self.elems[i]) + " "
                i += 1
                k += 1
            res += "\n"
            j *= 2
        return res

def main():
    pq = PQ()
    for i in xrange(9, -1, -1):
        pq.insert(i)
    print pq.peek()
    print pq.toString()
    for i in xrange(5):
        print pq.delmin()
    print pq.toString()

if __name__ == "__main__":
    main()
