import random

def partition(A, lo, hi):
    pivot = random.randint(lo, hi-1)
    tmp = A[pivot]
    A[pivot] = A[hi-1]
    A[hi-1] = tmp

    lower = lo
    upper = hi-2

    while lower <= upper:
        if A[lower] <= tmp:
            lower += 1
        else:
            t = A[lower]
            A[lower] = A[upper]
            A[upper] = t
            upper -= 1

    A[hi-1] = A[lower]
    A[lower] = tmp
    return lower

def qs(A, lo, hi):
    if lo >= hi:
        return

    mid = partition(A, lo, hi)

    qs(A, lo, mid)
    qs(A, mid+1, hi)

def qsort(A):
    qs(A, 0, len(A))

def randList(lo, hi, n):
    return map(lambda _: random.randint(lo, hi), range(n))
