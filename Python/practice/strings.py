def charArrayFromString(s):
    return map(lambda c: c, s)

def revseg(s, start, end):
    # Mistake: going to end actually just flips the whole thing around again,
    # giving back the original.
    # Mistake: Lots of indexing badness.
    for i in xrange((end-start)/2):
        tmp = s[start+i]
        s[start+i] = s[end-i-1]
        s[end-i-1] = tmp
    return s

def rev(s):
    revseg(s, 0, len(s))

    i = 0
    j = 0
    # Mistake: need to catch j at the end, not i.
    # Mistake: j can be len(s).
    while j <= len(s):
        if j == len(s) or s[j] == ' ':
            revseg(s, i, j)
            # Mistake: without this loop, the last word is offset if preceded
            # by multiple spaces.
            while j < len(s) and s[j] == ' ':
                j += 1
            i = j
        j += 1
    return s

def rev2(s):
    s = s[::-1]

    i = 0
    j = 0
    while j <= len(s):
        if j == len(s) or s[j] == ' ':
            # Mistake: this if case is necessary.
            if i == 0:
                s[i:j] = s[j-1::-1]
            else:
                # Original mistake: off by 1.
                s[i:j] = s[j-1:i-1:-1]
            while j < len(s) and s[j] == ' ':
                j += 1
            i = j
        j += 1
    return s

s = charArrayFromString("Hello there, human!")
print revseg(s, 0, len(s))
print revseg(s, 0, len(s))
print rev(s)
print rev2(charArrayFromString("Hello there, human!"))

print rev(charArrayFromString("I  like   spaces!"))
print rev2(charArrayFromString("I  like   spaces!"))
