// $ gcc -trigraphs evil.c

void fwtf (int n, int a??(n??)) ??<??>
void f0 (int n, int *a) {}
void f1 (int n, int a[n]) {}
void f2 (int n, int a[10]) {}
void f3 (int n, int a[42]) {}

int main () {
  int arg[10];
  f0(10, arg);
  f1(10, arg);
  f2(10, arg);
  f3(10, arg);
  fwtf(10, arg);
  return 0;
}
