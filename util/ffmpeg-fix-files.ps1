﻿param ([string]$pattern)

if ($pattern -eq "") {
  $usage = "Usage: " + $MyInvocation.MyCommand.Name + " <pattern>"
  echo $usage
  return
}

Get-ChildItem -Filter $pattern | Foreach-Object {
  ffmpeg -i "$_" -c:a copy tmp.m4a
  rm "$_"
  mv tmp.m4a "$_"
}