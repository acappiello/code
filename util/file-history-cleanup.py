import sys
import os

if len(sys.argv) != 2:
    print "usage %s <path>" % sys.argv[0]
    sys.exit(1)

path = sys.argv[1]

w = os.walk(path)

nkeep = 1

for root, dirs, files in w:
    print root
    bins = {}
    for f in files:
        # os.walk gets confused by paths with unicode characters.
        # This just skips them, doesn't actually fix anything...
        if not os.path.exists(os.path.join(root, f)):
            continue
        if f.count('.') > 0:
            (n, e) = f.rsplit('.', 1)
        else:
            n = f
            e = ''
        (n, _, _, _) = n.rsplit(' ', 3)
        if (n, e) in bins:
            bins[(n, e)].append(f)
        else:
            bins[(n, e)] = [f]
        #print n, e

    #print bins

    for k in bins:
        v = bins[k]
        v.sort()
        keep = v[-1 * nkeep:]
        delete = v[:-1 * nkeep]
        print 'keep:', keep
        print 'delete:', delete
        for d in delete:
            fullpath = os.path.join(root, d)
            #print fullpath
            #os.remove(fullpath)
            #os.system('rm -f "%s 2> /dev/null"' % fullpath)
            (drv, f) = os.path.splitdrive(fullpath)
            f = f.replace("\\", "/")
            if len(drv) == 0:
                cygpath = f
            else:
                cygpath = "/cygdrive/" + drv[0] + f
            os.system('rm -f "%s"' % cygpath)
