# File History cleanup, based on MD5.

import sys
import os
import hashlib

# http://stackoverflow.com/questions/1131220/get-md5-hash-of-big-files-in-python
def generate_file_md5(rootdir, filename, blocksize=2**20):
    m = hashlib.md5()
    with open( os.path.join(rootdir, filename) , "rb" ) as f:
        while True:
            buf = f.read(blocksize)
            if not buf:
                break
            m.update( buf )
    return m.hexdigest()

if len(sys.argv) == 1:
    path = "."
elif len(sys.argv) == 2:
    path = sys.argv[1]
else:
    print "usage %s [path]" % sys.argv[0]
    sys.exit(1)

w = os.walk(path)

for root, dirs, files in w:
    print root

    bins = {}

    for f in files:
        # os.walk gets confused by paths with unicode characters.
        # This just skips them, doesn't actually fix anything...
        if not os.path.exists(os.path.join(root, f)):
            continue
        if f.count('.') > 0:
            (n, e) = f.rsplit('.', 1)
        else:
            n = f
            e = ''
        (n, _, _, _) = n.rsplit(' ', 3)
        #md5 = hashlib.md5(open(os.path.join(root, f), 'rb').read()).hexdigest()
        md5 = generate_file_md5(root, f)

        if (n,e,md5) in bins:
            bins[(n,e,md5)].append(f)
        else:
            bins[(n,e,md5)] = [f]

    for k in bins:
        v = bins[k]
        v.sort()  # Keeps the oldest.
        if len(v) > 1:
            print k
            print 'keep:', v[0]
            print 'delete:', v[1:]
            for d in v[1:]:
                fullpath = os.path.join(root, d)
                #os.remove(fullpath)
                #os.system('rm -f "%s 2> /dev/null"' % fullpath)
                (drv, f) = os.path.splitdrive(fullpath)
                f = f.replace("\\", "/")
                if len(drv) == 0:
                    cygpath = f
                else:
                    cygpath = "/cygdrive/" + drv[0] + f
                os.system('rm -f "%s"' % cygpath)
