#! /usr/bin/env python

import os

for _, _, files in os.walk('.'):
    for f in files:
        n, e = f.rsplit('.', 1)
        if e != 'gif':
            continue
        _, i = n.rsplit(' ', 1)
        url = "https://i.imgur.com/%s.mp4" % (i)
        cmd = "curl -O %s" % (url)
        print cmd
        os.system(cmd)
