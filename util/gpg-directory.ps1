﻿param([string]$root = ".")

function GetFiles($dir, $outdir, [string[]]$exclude) 
{
    foreach ($item in Get-ChildItem $dir)
    {
        if ($exclude | Where {$item -like $_}) { continue }

        $file = Join-Path $dir $item
        $outfiledir = Join-Path $outdir $dir

        if (! (Test-Path $outfiledir)) {
            md $outfiledir| Out-Null
        }

        if (Test-Path $file -PathType Container) 
        {
            GetFiles $file $outdir $exclude
        } 
        else 
        { 
            $outfile = Join-Path $outdir $file
            $outfile = $outfile + ".gpg"

            # For now, don't refresh existing files.
            if (! (Test-Path $outfile)) {
                gpg -r "alex@alexcappiello.com" -o $outfile -e $file
            }
        }
    } 
}

$outroot = (Get-Item $root).Parent.FullName + "\gpg"

if (! (Test-Path $outroot)) {
    md $outroot | Out-Null
}

#$root
#$outroot
GetFiles $root $outroot
