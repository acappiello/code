# Find identical files, based on MD5.

import sys
import os
import hashlib

# http://stackoverflow.com/questions/1131220/get-md5-hash-of-big-files-in-python
def generate_file_md5(rootdir, filename, blocksize=2**20):
    m = hashlib.md5()
    with open( os.path.join(rootdir, filename) , "rb" ) as f:
        while True:
            buf = f.read(blocksize)
            if not buf:
                break
            m.update( buf )
    return m.hexdigest()

if len(sys.argv) == 1:
    path = "."
elif len(sys.argv) == 2:
    path = sys.argv[1]
else:
    print "usage %s [path]" % sys.argv[0]
    sys.exit(1)

w = os.walk(path)

for root, dirs, files in w:
    print root

    #data = [(hashlib.md5(open(os.path.join(root, fname), 'rb').read()).hexdigest(), fname) for fname in files]

    bins = {}

    for fname in files:
        #md5 = hashlib.md5(open(os.path.join(root, fname), 'rb').read()).hexdigest()
        md5 = generate_file_md5(root, f)
        if md5 in bins:
            bins[md5].append(fname)
        else:
            bins[md5] = [fname]

    for k in bins:
        v = bins[k]
        if len(v) > 1:
            print k, v
