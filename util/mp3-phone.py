# Change to OGG Vorbis.

import sys
import os

if len(sys.argv) != 2:
    print "usage %s <path>" % sys.argv[0]
    sys.exit(1)

path = sys.argv[1]

w = os.walk(path)

for root, dirs, files in w:
    print root
    for f in files:
        fullpath = os.path.join(root, f)
        if len(f) > 4 and f[-4:] == ".mp3" and os.path.exists(fullpath):
            print "running: %s" % (f)
            outname = os.path.join(root, "tmp.mp3")
            ffmpeg = 'ffmpeg -i "%s" -c:v copy -c:a libmp3lame -q:a 2 -map_metadata 0 -id3v2_version 3 "%s"' % (fullpath, outname)
            os.system(ffmpeg)
            os.remove(fullpath)
            os.rename(outname, fullpath)
        else:
            print "skipped: %s" % (f)
