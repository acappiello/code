#! /bin/bash

# http://wiki.bash-hackers.org/syntax/pe#substring_removal

for f in *
do
    echo $f
    base=${f%.*}
    #ext=${f##*.}
    file=$(file "${f}")
    if $(echo "${file#*:}" | grep -iq "audio")
    then
        ffmpeg -i "${f}" -c:v copy -c:a libmp3lame -q:a 0 -map_metadata 0 \
            -id3v2_version 3 "${base}.mp3"
    fi
done
