import sys
import os

if len(sys.argv) > 2:
    print "usage %s <path>" % sys.argv[0]
    sys.exit(1)

if len(sys.argv) == 1:
    path = "."
else:
    path = sys.argv[1]

w = os.walk(path)

for root, dirs, files in w:
    if root == path:
        continue

    print root

    for f in files:
        d = root.split(os.sep)[-1]
        src = root + os.sep + f
        dest = path + os.sep + d + "_" + f
        print "\t", f, src, dest
        os.rename(src, dest)

    os.rmdir(root)

