import os

path_string = os.environ["PATH"]

print(path_string)

path_list = path_string.split(";")

paths = {}
out = ""

for p in path_list:
    n = os.path.normpath(p)
    l = n.lower()
    if l in paths:
        paths[l] = paths[l] + 1
    else:
        paths[l] = 1
        out += n + ";"

keys = list(paths.keys())
keys.sort()
list(map(lambda k: print(k, paths[k]), keys))
print(out)
