﻿$OUTDIR       = "D:\Users\Alex\tmp\dvdrip\"
$STARTCHAPTER = 1
$ENDCHAPTER   = 16
$TITLE        = 8

cd $OUTDIR

$handbrake_path = ";C:\Program Files\Handbrake"
if ($env:path.IndexOf($handbrake_path) -eq -1) {
  $env:path += $handbrake_path
}

$STARTCHAPTER..$ENDCHAPTER | ForEach-Object {
  $OUTFILE = "$_" + ".mkv"
  $OUTAUDIO = "$_" + ".mp3"
  HandBrakeCLI.exe -i "F:\\" -t $TITLE --angle 1 -c $_ -o $OUTFILE  -f mkv -w 720 --crop 0:0:0:0 --loose-anamorphic --modulus 2 -e x264 -q 20 --vfr -a 1 -E copy -6 none -R Auto -B 0 -D 0 --gain 0 --audio-fallback ac3 --encoder-preset=veryfast --encoder-level="4.0" --encoder-profile=main #--verbose=1
  ffmpeg -i $OUTFILE -map 0:1 -acodec libmp3lame -q 0 $OUTAUDIO
  rm $OUTFILE
}