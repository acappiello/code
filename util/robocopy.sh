#! /bin/bash

DEST="H:/Users/Alex"

if [ ! -d ${DEST} ]
then
    mkdir -p ${DEST}
fi

for dir in Anime Desktop Documents Dropbox Encrypted IdeaProjects MEGAsync Music Pictures Programming "Saved Games" share tmp Videos "Virtual Machines" "VirtualBox VMs"
do
    robocopy "D:/Users/Alex/${dir}" "${DEST}/${dir}" /S /r:1 /w:1 /LOG:robocopy.log
done
