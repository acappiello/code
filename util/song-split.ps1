﻿$CONFIG = "I:\Music-Lossless\todd-rundgren-ridgefield.csv"
$SOURCE = "I:\Music-Lossless\todd-rundgren-ridgefield.flac"
$OUTDIR = "I:\Music-Lossless\conversion\"

$config = Get-Content ${CONFIG}

$i = 1

$config | ForEach-Object {
  if (![System.String]::IsNullOrWhiteSpace($_)) {
    $start, $end = $_.ToString().Split(",")
    $out = "${OUTDIR}${i}.mp3"
    echo ${out}
    if (-not (Test-Path ${out})) {
      ffmpeg -i ${SOURCE} -ss ${START} -to ${END} -metadata track="${i}" -metadata title="Track #${i}" -acodec libmp3lame -q 0 ${out}
    }
    $i += 1
  }
}
